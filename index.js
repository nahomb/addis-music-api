const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const globalErroHandler = require("./controller/errorController")
const AppError = require('./utils/AppError')
const songRouter = require('./routes/songRoutes')
const app = express();

dotenv.config(path.join(__dirname,'./.env'))
app.use(morgan('dev'));
app.use(cors({ origin:"*"}))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


mongoose.connect(process.env.MONGO_URL, { }).then(() => console.log("DB connection successful!"));
 
app.use('/songs', songRouter);

app.use('*',(req, res, next)=>{
    next(new AppError(`can't find the requested url ${req.originalUrl} on this server`, 404));
})
 
app.use(globalErroHandler);
const port = process.env.PORT || 9002;

app.listen(port, ()=>{
    console.log(`server is up and running on port ${port}`);
})