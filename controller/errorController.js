 const errorController = (err, req, res, next) =>{
    let statusCode = err.statusCode || 500;
    let status = err.status || 'error';

    res.status(statusCode).json({
        status: err.status,
        message: err.message
    })
}

module.exports = errorController;