const catchAsync = require( "../utils/catchAsync");

const factory = require("./factory"); 
const Song = require("./../models/songModel")

exports.getAllSongs = factory.getAll(Song);
exports.getSong = factory.getOne(Song);
exports.createSong = factory.createOne(Song);
exports.updateSong = factory.updateOne(Song);
exports.deleteSong = factory.deleteOne(Song);

exports.getStat = catchAsync( async (req,res, next)=>{
    
    const stat =   Song.aggregate([
        {
          $match: {
            ...req.query
          }
        }, {
          $facet: {
            'songs': [
              {
                $count: 'title'
              }
            ], 
            'album': [
              {
                $count: 'album'
              }
            ], 
            'genre': [
              {
                $count: 'genre'
              }
            ]
          }
        }
      ])
    let data = await stat.exec();

    data = {
        songs: data[0].songs[0].title,
        album: data[0].album[0].album,
        genre: data[0].genre[0].genre

    }
    console.log('data',data);
    res.status(200).json({
        status: 'success',
        message: data
    })
}) 