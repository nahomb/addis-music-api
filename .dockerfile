FROM node:alpine:3.17
WORKDIR /usr/src/app

ARG PORT

EXPOSE ${PORT}

RUN apk add yarn

COPY package.json yarn.lock ./

RUN yarn install

COPY index.js .
 
COPY . .

CMD ["node", "index.js"]
